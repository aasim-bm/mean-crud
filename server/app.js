const express = require("express");
const dotenv = require("dotenv");
const bodyParser = require("body-parser");

const connectDB = require("./config/db");
const { empRoute } = require("./routes/index");

// Load config
dotenv.config({ path: "./config/config.env" });

const app = express();
// MIDDLEWARES

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }));

// Routes
app.use("/api", empRoute);

connectDB();

app.listen(3000);
