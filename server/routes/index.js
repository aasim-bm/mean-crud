const express = require("express");

const {
  getEmps,
  saveEmp,
  getEmpById,
  updateEmpById,
  deleteEmpById,
} = require("../controllers/employees");

const empRoute = express.Router();

// Get all
empRoute.get("/employees", getEmps);

// Save
empRoute.post("/add", saveEmp);

// Get single
empRoute.get("/employee/:id", getEmpById);

// Update
empRoute.put("/employee/edit/:id", updateEmpById);

// Delete
empRoute.delete("/employee/delete/:id", deleteEmpById);

module.exports = { empRoute };
