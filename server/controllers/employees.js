const mongoose = require("mongoose");

const { Employee } = require("../models/employees");

// Get All
const getEmps = async (req, res) => {
  try {
    const emps = await Employee.find();
    res.status(200).json(emps);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

// Save
const saveEmp = async (req, res) => {
  const { name, email, salary } = req.body;
  const newEmp = new Employee({ name, email, salary });
  try {
    await newEmp.save();
    res.status(201).json(newEmp);
  } catch (error) {
    res.status(409).json({ message: error.message });
  }
};

// Get single
const getEmpById = async (req, res) => {
  const id = req.params.id;

  try {
    const emp = await Employee.findById(id);
    res.status(200).json(emp);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

// Update
const updateEmpById = async (req, res) => {
  const id = req.params.id;

  const { name, salary, email } = req.body;
  const emp = { name, salary, email };

  try {
    await Employee.findByIdAndUpdate(id, { $set: emp }, { new: true });
    res.status(200).json(emp);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

const deleteEmpById = async (req, res) => {
  const id = req.params.id;

  try {
    await Employee.findByIdAndRemove(id);
    res.status(200).json({ message: "employee removed" });
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

module.exports = { getEmps, saveEmp, getEmpById, updateEmpById, deleteEmpById };
